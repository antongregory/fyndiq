package com.test.n10.fyndiq_case.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.test.n10.fyndiq_case.model.helper.ResponseMap;
import com.test.n10.fyndiq_case.model.helper.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by N10 on 7/24/2016.
 */

public class PostService extends Service {

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        
        startTimer();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }


    private void checkForUpdate(){
        if(!ResponseMap.responseMap.isEmpty()){
            new PostStatus(ResponseMap.responseMap).execute();
        }
        else{
            startTimer();
        }
    }

    private void startTimer(){

        new Handler().postDelayed(new Runnable() {
            public void run() {

                checkForUpdate();
            }
        }, 5000);
    }
    public class PostStatus extends AsyncTask<Void, Map<Integer,String>, Void> {


        private Map<Integer , String> responseMap ;
        Iterator iterator;
        JSONObject jsonObject;
        public PostStatus(Map<Integer,String> map){
            this.responseMap=new HashMap<Integer,String>(map);;
        }

        @Override
        protected void onPreExecute() {

            iterator=responseMap.entrySet().iterator();

        }

        @Override
        protected Void doInBackground(Void... params) {

            try {



                JSONArray jsonArray=new JSONArray();


                while(iterator.hasNext()){
                    jsonObject=new JSONObject ();
                    Map.Entry<Integer, String> entry = (Map.Entry)iterator.next();
                    jsonObject.put("id",entry.getKey());
                    jsonObject.put("response",entry.getValue());
                    jsonArray.put(jsonObject);
                }
            Utils.putDataToServer(Utils.URL,jsonArray);

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

            return null;
        }



        @Override
        protected void onPostExecute(Void result) {
            ResponseMap.clearMap(responseMap);
            startTimer();
        }


    }


}
