package com.test.n10.fyndiq_case.model.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.test.n10.fyndiq_case.R;
import com.test.n10.fyndiq_case.model.helper.ResponseMap;
import com.test.n10.fyndiq_case.model.helper.Utils;
import com.test.n10.fyndiq_case.model.pojo.Product;

import java.util.List;



public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {



    List<Product> productList;
    Context mContext;

    public ProductListAdapter(Context context, List<Product> list) {
        super();
        mContext=context;
        productList=list;

    }



    public void addProduct(Product product) {
        productList.add(product);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View row = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_row, viewGroup, false);

        return new ViewHolder(row);

    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {

        final Product product = productList.get(position);

        viewHolder.id.setText(Integer.toString(product.getId()));
        viewHolder.title.setText(product.getTitle());

        setmImage(product.getUrl(),viewHolder.imgThumbnail);

        viewHolder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             product.setResponse(Utils.LIKE);
                ResponseMap.responseMap.put(product.getId(),product.getResponse());
                Toast.makeText(mContext,"like",Toast.LENGTH_LONG).show();

            }
        });

        viewHolder.dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                product.setResponse(Utils.DISLIKE);
                ResponseMap.responseMap.put(product.getId(),product.getResponse());
                Toast.makeText(mContext,"dislike",Toast.LENGTH_LONG).show();

            }
        });


        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;

                Context context = v.getContext();
                String title=viewHolder.title.getText().toString();

                Toast.makeText(mContext,title,Toast.LENGTH_LONG).show();

            }
        });

    }

    public void setmImage(String url,ImageView mImage)  {

        Glide.with(mImage.getContext())
                .load(url)
                .fitCenter()
                .into(mImage);
    }
    @Override
    public int getItemCount() {

        return productList.size();


    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imgThumbnail;
        public Button like,dislike;
        public TextView title;
        public TextView id;

        public View mView;

        public ViewHolder(View itemView) {
            super(itemView);
            mView=itemView;

            title=(TextView)mView.findViewById(R.id.title);
            id=(TextView)mView.findViewById(R.id.idnumber);
            imgThumbnail=(ImageView)mView.findViewById(R.id.thumbnail);
            like= (Button) mView.findViewById(R.id.like);
            dislike= (Button) mView.findViewById(R.id.dislike);

        }
    }





}
