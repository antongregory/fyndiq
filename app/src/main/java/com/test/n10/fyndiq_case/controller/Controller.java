package com.test.n10.fyndiq_case.controller;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import com.test.n10.fyndiq_case.model.helper.Utils;
import com.test.n10.fyndiq_case.model.pojo.Product;
import com.test.n10.fyndiq_case.service.PostService;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * Created by N10 on 7/23/2016.
 */

public class Controller {

    private Context context;
    private  ProductSyncListener productListener;
    public  List<Product> listMapper;
        public Controller(Context mContext,ProductSyncListener productSyncListener){
            context=mContext;
            productListener=productSyncListener;
        }


    public void initiateService() {
        context.startService(new Intent(context, PostService.class));
    }


    public interface ProductSyncListener{

        void onSyncProgress(String status);
        void onSyncFinish(List<Product> list);
        void onSyncFailed();

    }

    public void onStartSync(List<Product> productList){
        listMapper=productList;
        new ListFetcher().execute();
    }

    private class ListFetcher extends AsyncTask<Void, Void, List<Product>> {


        Product productObject;
        @Override
        protected void onPreExecute() {



            if(Utils.isConnected(context)){
                productListener.onSyncProgress("\tLoading...");
            }
            else{
                productListener.onSyncFailed();
            }
        }

        @Override
        protected List<Product> doInBackground(Void... params) {

            try {

                JSONArray jsonArray=Utils.parseEntireData(Utils.URL);
                JSONObject jsonObject;
                if (jsonArray!=null){
                    for (int i=0;i<jsonArray.length();i++){
                        jsonObject= (JSONObject) jsonArray.get(i);
                        productObject=new Product();
                        productObject.setId(jsonObject.getInt("id"));
                        productObject.setTitle(jsonObject.getString("name"));
                        productObject.setUrl(jsonObject.getString("image"));
                        listMapper.add(productObject);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return listMapper;
        }

        @Override
        protected void onPostExecute(List<Product> result) {


            productListener.onSyncFinish(result);
        }
    }



}
