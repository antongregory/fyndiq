package com.test.n10.fyndiq_case.viewUI;


        import android.app.ProgressDialog;
        import android.content.Context;
        import android.content.DialogInterface;
        import android.support.design.widget.Snackbar;
        import android.support.v7.app.AlertDialog;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.support.v7.widget.LinearLayoutManager;
        import android.support.v7.widget.RecyclerView;
        import android.support.v7.widget.Toolbar;

        import com.test.n10.fyndiq_case.R;
        import com.test.n10.fyndiq_case.controller.Controller;
        import com.test.n10.fyndiq_case.model.adapter.ProductListAdapter;
        import com.test.n10.fyndiq_case.model.pojo.Product;

        import java.util.ArrayList;
        import java.util.List;

public class MainActivity extends AppCompatActivity implements Controller.ProductSyncListener{

    List<Product> listMapper;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    Context mContext;
    private Controller mController;
    ProgressDialog loading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outline_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mRecyclerView = (RecyclerView)findViewById(R.id.main_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mContext=getApplicationContext();
        loading = new ProgressDialog(this);
        mController=new Controller(mContext,this);
        listMapper=new ArrayList<Product>();
        mController.onStartSync(listMapper);
        mController.initiateService();

    }

    @Override
    public void onSyncFinish(List<Product> list){
        loading.dismiss();
        listMapper=list;
        mAdapter = new ProductListAdapter(mContext,listMapper);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onSyncProgress(String status){
        loading.setMessage(status);
        loading.show();

    }

    @Override
    public void onSyncFailed(){
        showSnackBar();
    }

    private void showSnackBar()
    {


        Snackbar.make(mRecyclerView, "Cant connect to internet", Snackbar.LENGTH_LONG).show();

    }






    }




