package com.test.n10.fyndiq_case.model.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.test.n10.fyndiq_case.model.pojo.Product;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONStringer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.SocketException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;

/**
 * Created by N10 on 7/23/2016.
 */

public class Utils {

    public static List<Product> responseList;
    public static final String DISLIKE="dislike";
    public static final String LIKE="like";

    public static final String URL="http://104.155.37.85/interview/products";

    public static void addResponse(Product product){
        responseList.add(product);
    }

    public static void clear(){
        responseList.clear();
    }


    public static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifiInfo != null && wifiInfo.isConnected()) || (mobileInfo != null && mobileInfo.isConnected()))
            return true;
        else
            return false;
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
    public static JSONArray parseEntireData(String url) throws JSONException, IOException {


        InputStream is = new URL(url).openStream();
        JSONArray jsonArray = null;
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            jsonArray = new JSONArray(jsonText);
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            is.close();
        }
        return jsonArray;

    }

    public  static String putDataToServer(String url,JSONArray returnedJObject) throws Throwable
    {

        HttpPost request = new HttpPost(url);
        JSONStringer json = new JSONStringer();
        StringBuilder sb=new StringBuilder();
        int i=0;


        if (returnedJObject!=null)
        {


            while (i<returnedJObject.length())
            {
                json.object();

                json.key("id").value(returnedJObject.getJSONObject(i).get("id"));
                json.key("response").value(returnedJObject.getJSONObject(i).get("response"));
                json.endObject();

                i++;
            }
        }
        json.endArray();



        StringEntity entity = new StringEntity(json.toString());

        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,"application/json;charset=UTF-8"));
        request.setHeader("Accept", "application/json");
        request.setEntity(entity);

        HttpResponse response =null;
        DefaultHttpClient httpClient = new DefaultHttpClient();

        HttpConnectionParams.setSoTimeout(httpClient.getParams(), 10000);
        HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 10000);
        try{

            response = httpClient.execute(request);


        }
        catch(SocketException se)
        {

            throw se;
        }

        InputStream in = response.getEntity().getContent();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line = null;
        while((line = reader.readLine()) != null){
            sb.append(line);

        }

        return sb.toString();
    }


}
