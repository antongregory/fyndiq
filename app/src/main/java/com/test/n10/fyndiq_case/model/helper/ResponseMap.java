package com.test.n10.fyndiq_case.model.helper;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by N10 on 7/24/2016.
 */

public class ResponseMap {

    public static Map<Integer,String> responseMap=new HashMap<Integer,String>();

    public boolean isEmpty(){

        return responseMap.isEmpty();
    }

    public void putResponse(int id,String response){

        responseMap.put(id,response);

    }

    public static void clearMap(Map<Integer,String> sentMap){
        Iterator iterator=sentMap.entrySet().iterator();
        Log.d("timern10", "about to remove");
        while(iterator.hasNext()) {
            Map.Entry<Integer, String> sentEentry = (Map.Entry) iterator.next();
            if (responseMap.containsKey(sentEentry.getKey())) {
                if (responseMap.get(sentEentry.getKey()).equals(sentEentry.getValue()))
                    responseMap.remove(sentEentry.getKey());
                }
            }
        }
}
