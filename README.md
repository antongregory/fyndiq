# Task given by Fyndiq
##Author
Anton Gregory.  
Email: [antonrose.g@gmail.com](Link URL)

##Features done:
* Fetches products from API
* Post the number of likes in batches every 5 seconds
* Made use of MVC pattern
* Added like and dislike button.